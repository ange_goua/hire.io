const mongoose = require('mongoose')

//Database
mongoose.connect(process.env.DB_CONNECT, 
    {useNewUrlParser: true, useUnifiedTopology: true}, 
    ()=> {
    console.log('Connected to db');
})