const mongoose = require('mongoose')

const CandidateSchema = mongoose.Schema(
    {
        lastname: {
            type: String,
            required: true
        },
        firstname: {
            type: String,
            required: true
        },
        email: {
            type: String,
            required: true
        },
        motivation: {
            type: String,
            required: true
        },
        job: {type: String},
    },
    { timestamps: true }
)

module.exports = mongoose.model('Candidate', CandidateSchema)