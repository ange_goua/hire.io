const mongoose = require('mongoose')
// const Job = require('./job.model')

const UserSchema = mongoose.Schema(
    {
        email: {
            type: String,
            required: true
        },
        username: {
            type: String,
        },
        password: {
            type: String,
            required: true,
            max: 1024,
            min: 6
        },
    },
    { timestamps: true }
)



module.exports = mongoose.model('User', UserSchema)