const mongoose = require('mongoose')

const JobSchema = mongoose.Schema(
    {
        author: {type: mongoose.Schema.Types.ObjectId, ref: "User"},
        title: {
            type: String,
            required: true
        },
        location: {
            type: String,
            required: true
        },
        contractType: [String],
        description: String,
        date: {
            type: Date,
            default: Date.now()
        }
    },
    { timestamps: true }
)

module.exports = mongoose.model('Job', JobSchema)