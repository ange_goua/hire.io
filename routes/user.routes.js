const express = require('express');
const router = express.Router()
const bcrypt = require('bcryptjs');
const User = require('../database/models/user.model')
const { registerValidation, loginValidation } = require('../validation');
const jwt = require('jsonwebtoken');


//Routes
router.get('/', (req, res, next) => {
    res.send('toto')
})

router.post('/register', async (req, res, next) => {
    const body = req.body
    const { error } = registerValidation(body);
    if (error) return res.status(400).send(error.details[0].message);

    //Check if email is already in the DB
	const emailExist = await User.findOne({ email: req.body.email });
    if (emailExist) return res.status(400).send('Email already used')
    
    //Hash password
	const salt = await bcrypt.genSalt(10);
	const hashedPassword = await bcrypt.hash(body.password, salt);
    
    const user = new User({
        username: body.username,
        password: hashedPassword,
        email: body.email,
    })

    try {
		const savedUser = await user.save();
		res.send(savedUser);
	} catch (err) {
		res.status(401).send(err);
	}

})

router.post('/login', async (req, res) => {
	const { error } = loginValidation(req.body);
	if (error) return res.status(400).send(error.details[0].message);
	//Check if email-password
	let user = await User.findOne({ email: req.body.email });
	if (!user) return res.status(400).send('Invalid Email');

	const validPass = await bcrypt.compare(req.body.password, user.password);
	if (!validPass) return res.status(400).send('Invalid Password');

	//Assign Token
	const token = jwt.sign({ _id: user._id }, process.env.TOKEN);
	res.setHeader('auth-token', token)

	// window.localStorage.setItem('auth-token', token)
	res.send(token);
	
	console.log('Connected')
    // res.send(user);
});

module.exports = router