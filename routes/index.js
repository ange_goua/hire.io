const router = require('express').Router()
const jobsRoutes = require('./jobs.routes')
const userRoutes = require('./user.routes')
const candidatesRoutes = require('./candidates.routes')
const profileRoutes = require('./profile.routes')

router.use('/user', userRoutes)
router.use('/user/profile', profileRoutes)
router.use('/v1/api/jobs', jobsRoutes)
router.use('/v1/api/apply', candidatesRoutes)
// router.use('/', (req, res) => {res.send('Hire.io/ Connexion Ok')})


module.exports = router
