const express = require('express');
const router = express.Router()
const Job = require('../database/models/job.model')
// const User = require('../database/models/user.model')
const Candidate = require('../database/models/candidate.model')
const verify = require('../verifyToken');


//Apply to a job
router.post('/:jobID', async (req, res, next) => {
    const body = req.body
    const job = await Job.findOne({_id: req.params.jobID},)
    const jobId = job._id

    try {
        const candidate = new Candidate ({
            firstname: body.firstname,
            lastname: body.lastname,
            motivation: body.motivation,
            email: body.email,
            job: jobId,
        })
        candidate.save()
        res.json(jobId)

    } catch (error) {
      res.json({message: error})
    }

    
})

//Get Candidates
router.get('/:jobID', async (req, res, next) => {
    try {
      Candidate
        .find({job: req.params.jobID})
        .then(posts => res.status(200).json(posts))
  } catch (e) {
    next(e)
  }
})

module.exports = router