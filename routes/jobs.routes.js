const express = require('express');
const router = express.Router()
const Job = require('../database/models/job.model')
const User = require('../database/models/user.model')
const verify = require('../verifyToken');

//Routes

//Get all post from the logged in user 
router.get('/', verify, (req, res, next) => {
    try {
      Job
        .find({author: req.user._id},)
        .then(posts => res.status(200).json(posts))
  } catch (e) {
    next(e)
  }

})

router.get('/all', (req, res, next) => {
    try {
      Job
        .find()
        .then(posts => res.status(200).json(posts))
  } catch (e) {
    next(e)
  }

})

//Create post for the logged in user
router.post('/', verify, async (req, res, next) => {
    const body = req.body
    const user = await User.findOne({ _id: req.user._id });

    const job = new Job({
        title: body.title,
        location: body.location,
        contractType: body.contractType,
        description: body.description,
        author: user,
    })
    await job
        .save()
        .then(data => {
            res.json(job);
        })
        .catch(err => {
            res.json({message : err})
        })
})

//update post for the logged in user
router.patch('/:jobID', verify, async (req, res, next) => {
    const body = req.body
    // const userJobs = await Job.find({author: req.user._id},)

    try {
      const updatedJob = await Job.updateOne(
        {_id: req.params.jobID},
        {$set: {
          title: body.title,
          location: body.location,
          contractType: body.contractType,
          description: body.description,
        }}
      )
      res.send(updatedJob)
    } catch (error) {
      res.json({message: error})
    }
  
})

//delete post for the logged in user
router.delete('/:jobID', verify, async (req, res, next) => {
    const body = req.body
    // const userJobs = await Job.find({author: req.user._id},)

    try {
      const deletedJob = await Job.deleteOne(
        {_id: req.params.jobID},
      )
      res.send(deletedJob)
    } catch (error) {
      res.json({message: error})
    } 
})

module.exports = router