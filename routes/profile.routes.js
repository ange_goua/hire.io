const router = require('express').Router();
const verify = require('../verifyToken');
const User = require('../database/models/user.model')



router.post('/', verify, async (req, res) => {
	try {
		let user = await User.findOne({ _id: req.user._id });
		res.send(user);
	} catch (error) {
		res.status(400).send(error);
	}
});

module.exports = router;
