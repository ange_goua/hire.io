import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
// import router from '../routes/routes'
Vue.use(Vuex)

const url = 'http://localhost:3000/'


const user = {
  namespaced: true,
  state: {
    datas: [],
    errors: []
  },

  actions: {
    async trySignIn(context, user) {
      try {
        await axios.post(`${url}user/login`, user)
        context.commit('signInSuccess')
        console.log("tata")

      } catch (error) {
        context.commit('signInError')
      }
    }
  },

  mutations: {
    signInSuccess() {
      console.log('Sign Up Success');
    },
    signInError(error) {
      console.log('Sign Up Error', error);
      // state.errors = error.response.data
    }
  }
}



const post = {
  namespaced: true,
  state: {
    datas: [],
    isLoading: false,
    errors: []
  },
  actions: {
    async tryGetPosts(context) {
      try {
        const response = await axios.get(`${url}v1/api/jobs/all`)
        const data = response.data
        context.commit('addPostContent', data)
      } catch (e) {
        context.commit('postGotError')
      }
    },

    // async tryPublish(context, post) {
    //   try {
    //     await axios.post(`${url}api/post`, post)
    //     context.commit('postPublishSuccess')
    //     router.push('/')
    //   } catch (error) {
    //     context.commit('postPublishError')
    //   }
    // }
  },

  mutations: {
    addPostContent(state, postContents) {
      state.datas = postContents.reverse()
      console.log(postContents)
    },
    postGotSuccess() {
      console.log('Post got ! Success')
    },

    postGotError() {
      console.log('Post didnt get ! Error')
    },
    postPublishSuccess() {
      console.log('Success')
    },

    postPublishError() {
      console.log('Error')
    }
  }
}



const store = new Vuex.Store({
  modules: {
    post,
    user
  }
})

export default store
