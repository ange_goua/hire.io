# Hire.io

Hire.io is an API which allow you to manage your jobs offers, and publish it fluently and easily

### How it works

You have to create an account on our platform https://hire-app0.herokuapp.com/ and get your crdentials. You can manage your data directly from your dashboard. You will be able to CREATE, UPDATE, DELETE, and GET all datas you need. Also, you can reach your datas remotely to our website from our API. 

#### *v1*
## Routes

### AUTH

- Register  
`POST`: `https://hire-app0.herokuapp.com/user/register`  

- Login  
`POST`: `https://hire-app0.herokuapp.com/user/login`

- User Profile datas  
`GET`: `https://hire-app0.herokuapp.com/user/profile`

### JOBS DATAS  
- Create a new job offer   
`POST`: `https://hire-app0.herokuapp.com/v1/api/jobs`  

- Get all jobs offers you have created  
`GET`: `https://hire-app0.herokuapp.com/v1/api/jobs`

- Update one of your jobs offers  
`PATCH`: `https://hire-app0.herokuapp.com/v1/api/jobs/:jobID`

- Delete one of your jobs offers  
`DELETE`: `https://hire-app0.herokuapp.com/v1/api/jobs/:jobID`

### CANDIDATES
- Get all candidates who apply to an offer  
`GET`: `https://hire-app0.herokuapp.com/v1/api/apply/:jobID`

- To allow applying to a job  
`POST`: `https://hire-app0.herokuapp.com/v1/api/apply/:jobID`


#### *v1* (Coming soon)

- Possibility to connect your data to others hiring platform like https://indeed.com, https://welcometothejungle.com  
- Possiblity to to see the status of your job offer : you will be able to close without delete, pause, and open existing job offer


## Terms and conditions

#### Section 1: Account and Registration

a. Accepting the Terms
You may not use the APIs and may not accept the Terms if (a) you are not of legal age to form a binding contract with Google, or (b) you are a person barred from using or receiving the APIs under the applicable laws of the United States or other countries including the country in which you are resident or from which you use the APIs.

b. Entity Level Acceptance
If you are using the APIs on behalf of an entity, you represent and warrant that you have authority to bind that entity to the Terms and by accepting the Terms, you are doing so on behalf of that entity (and all references to "you" in the Terms refer to that entity).

c. Registration
In order to access certain APIs you may be required to provide certain information (such as identification or contact details) as part of the registration process for the APIs, or as part of your continued use of the APIs. Any registration information you give to Google will always be accurate and up to date and you'll inform us promptly of any updates.

d. Subsidiaries and Affiliates
Google has subsidiaries and affiliated legal entities around the world. These companies may provide the APIs to you on behalf of Google and the Terms will also govern your relationship with these companies.
